.global _main 
				

_main:                      ;como son 7 segmentos se establecen los pines de salida
	BCLR	TRISB,	#15 ;Segmanto A pata 26
	BCLR	TRISB,	#14 ;Segmento B pata 25 
	BCLR	TRISB,	#13 ;Segmento C pata 24
	BCLR	TRISB,	#11 ;Segmento D pata 22
	BCLR	TRISB,	#9 ;Segmento E pata 18
	BCLR	TRISB,	#8 ;Segmento F pata 17
	BCLR	TRISB,	#7 ;Segmento G pata 16
	

	
	MOV	#0X000, W0	; Se pone el registro W0 en cero
	MOV	W0, PORTB	;Todos los valores del PORTB SE PONEN EN CERO
	
_bucle:	


; NUMERO 1
    ;Se encenderian unicamente los segmentos B Y C
	BSET	LATB,	#13; Se asigna un 1
	BSET	LATB,	#14
	CALL	DELAY1S
	BCLR	LATB,	#13 ;Despues de 1 seg hay un clear
	BCLR	LATB,	#14
	CALL	DELAY1S
; Numero 2
	; Se encienden los segmentos A, B,G,E,y D
    	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#7
	BSET	LATB,	#9
	BSET	LATB,	#11
	CALL	DELAY1S    ; Se mantienen encendidos durante 2 segundos
	CALL	DELAY1S
    	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#7
	BCLR	LATB,	#9
	BCLR	LATB,	#11 ; se resetean
	CALL	DELAY1S   

; Numero 3
       ; Se encienden los segmentos A,B,G,C,y D
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#7
	BSET	LATB,	#13
	BSET	LATB,	#11
	CALL	DELAY1S      ; Se prende durante 3 segundos
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#7
	BCLR	LATB,	#13
	BCLR	LATB,	#11 ; se resetea lo que hay en port B
	CALL	DELAY1S

; Numero 4
	;Se encienden los segmentos F,G,B yC
	
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#14
	BSET	LATB,	#13
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S  ; delay de 4 segundos porque es el numero 4
	CALL	DELAY1S
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#14
	BCLR	LATB,	#13 ; se resetean los valores 
	CALL	DELAY1S

; Numero 5
	;Se encienden A, F,G,C yD

	BSET	LATB,	#15
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#13
	BSET	LATB,	#11
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S    
	CALL	DELAY1S
	CALL	DELAY1S ; 5 delays porque corresponde al num 5
	BCLR	LATB,	#15
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#13 ; se ponen en ceros estos segmentos
	BCLR	LATB,	#11
	CALL	DELAY1S

; Numero 6 
	; Se enciende A, C,D,E,F y G

	BSET	LATB,	#15
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	BSET	LATB,	#7
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	CALL	DELAY1S

; Numero 7
	;Se enciende A, B y C

	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	CALL	DELAY1S

; Numero 8	
    ; Se encienden todos
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	BSET	LATB,	#7
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	CALL	DELAY1S

; Numero 9
    ;Se encienden tos menos el SEGMENTLO D y E
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#13
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#13
	CALL	DELAY1S
	
;Numero 0 
	;Se encienden todos menos el SEGMNTO G
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	CALL	DELAY1S
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	CALL	DELAY1S
	

	GOTO	_bucle		;Una vez termina la intrucción vuelve al inicio
;Calibracion de delays tomada del primer lab
DELAY1S:
	    MOV	    #1000,W0
REPETIR:
	    DEC	    W0, W1
	    MOV	    W1, W0
	    BRA	    Z, RETORNAR
	    REPEAT  #5000
	    NOP
	    GOTO    REPETIR
RETORNAR:
	    RETURN 
	    
.END
